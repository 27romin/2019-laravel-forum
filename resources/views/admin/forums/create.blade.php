@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>Create Forum</h2>
	@include('admin.templates.partials.messages.errors')

	<form action="{{ route('admin.forum.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset>
			<legend>Name</legend>
			<input type="text" name="name" value="{{ old('name') ? old('name') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<input type="text" name="description" value="{{ old('description') ? old('description') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Position</legend>
			<input type="text" name="position" value="{{ old('position') ? old('position') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Category</legend>
			@if(!$categories->isEmpty())
				<select name="category_id" id="category_id">
					@foreach($categories as $category)
						<option value="{{ $category->id }}" @if(old('category_id') == $category->id) selected @endif>{{ $category->name }}</option>
					@endforeach
				</select>
			@else
				<a href="{{ route('admin.forum.category.create') }}">Create Category</a>
			@endif
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection
