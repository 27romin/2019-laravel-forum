@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>Edit Category</h2>
	@include('admin.templates.partials.messages.errors')

	<form action="{{ route('admin.forum.category.update', $category) }}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<fieldset>
			<legend>Name</legend>
			<input type="text" name="name" value="{{ $category->name }}">
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<input type="text" name="description" value="{{ $category->description }}">
		</fieldset>
		<fieldset>
			<legend>Position</legend>
			<input type="text" name="position" value="{{ $category->position }}">
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection
