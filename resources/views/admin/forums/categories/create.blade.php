@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>Create Category</h2>
	@include('admin.templates.partials.messages.errors')

	<form action="{{ route('admin.forum.category.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset>
			<legend>Name</legend>
			<input type="text" name="name" value="{{ old('name') ? old('name') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<input type="text" name="description" value="{{ old('description') ? old('description') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Position</legend>
			<input type="text" name="position" value="{{ old('position') ? old('position') : '' }}">
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection
