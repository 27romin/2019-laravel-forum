@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>Edit Forum</h2>
	@include('admin.templates.partials.messages.errors')

	<form action="{{ route('admin.forum.update', $forum) }}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<fieldset>
			<legend>Name</legend>
			<input type="text" name="name" value="{{ $forum->name }}">
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<input type="text" name="description" value="{{ $forum->description }}">
		</fieldset>
		<fieldset>
			<legend>Position</legend>
			<input type="text" name="position" value="{{ $forum->position }}">
		</fieldset>
		<fieldset>
			<legend>Category</legend>
			@if(!$categories->isEmpty())
				<select name="category_id" id="category_id">
					@foreach($categories as $category)
						<option value="{{ $category->id }}" @if($forum->category->id == $category->id) selected @endif>{{ $category->name }}</option>
					@endforeach
				</select>
			@endif
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection
