@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<p>
		@include('admin.templates.partials.messages.success')
	</p>
	<h2>Categories</h2>
	<p>
		<a href="{{ route('admin.forum.category.create') }}">Create Category</a>
	</p>
	@if(!$categories->isEmpty())
		<fieldset>
			@foreach ($categories as $category) 
				{{ $category->name }} - <a href="{{ route('admin.forum.category.edit', $category) }}">edit</a>
				<form action="{{ route('admin.forum.category.destroy', $category) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button type="submit">delete</button>
				</form>
				<hr>
			@endforeach
		</fieldset>
	@endif

	<h2>Forums</h2>
	<p>
		<a href="{{ route('admin.forum.create') }}">Create Forum</a>
	</p>
	@foreach ($categories as $category) 
		<fieldset>
			<legend>{{ $category->name }}</legend>
			@foreach ($category->forums as $forum) 
				{{ $forum->name }} - <a href="{{ route('admin.forum.edit', $forum) }}">edit</a>
					<form action="{{ route('admin.forum.destroy', $forum) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit">delete</button>
						@if(!$forum->subforums->isEmpty())
							<p>
								<strong>Subforums</strong>
							</p>
							@foreach($forum->subforums as $subforum)
								<fieldset>
									<a href="{{ route('admin.forum.subforum.show', $subforum) }}">{{ $subforum->forum->name }}</a> 
								</fieldset>
							@endforeach
						@endif
					</form>

					<a href="{{ route('admin.forum.subforum.create', $forum) }}">Add subforum</a>
				<hr>
			@endforeach
		</fieldset>
	@endforeach
@endsection
