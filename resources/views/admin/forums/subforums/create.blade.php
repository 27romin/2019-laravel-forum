@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>Create Subforum</h2>
	@include('admin.templates.partials.messages.errors')

	<fieldset>
		<legend>Select Forum</legend>
		<form action="{{ route('admin.forum.subforum.store', $forum) }}" method="POST">
			{{ csrf_field() }}
			<select name="child_id" id="child_id">
				<option value="">Select a forum</option>
				@foreach($nonSubForums as $nonSubForum)
					@if($nonSubForum->id != $forum->id)
						<option value="{{ $nonSubForum->id }}" @if(old('child_id') == $nonSubForum->id) selected @endif>{{ $nonSubForum->name }}</option>
					@endif
				@endforeach
			</select>
			<button type="submit">Submit</button>
		</form>
	</fieldset>
@endsection
