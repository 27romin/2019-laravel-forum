@extends('app.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
	<h2>View Subforum</h2>
	@include('admin.templates.partials.messages.errors')

	<fieldset>
		<legend>{{ $subforum->forum->name }}</legend>
		<form action="{{ route('admin.forum.subforum.destroy', $subforum) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<button type="submit">delete</button>
		</form>
	</fieldset>
@endsection
