<h3>Create Post</h3>
@include('app.templates.partials.messages.errors')
<form action="{{ route('app.forum.post.store', $thread) }}" method="POST">
	{{ csrf_field() }}
	<fieldset>
		<legend>Body</legend>
		<textarea name="body" id="body" cols="30" rows="10">{{ old('body') ? old('body') : '' }}</textarea>
	</fieldset>
	<button type="submit">Submit</button>
</form>
