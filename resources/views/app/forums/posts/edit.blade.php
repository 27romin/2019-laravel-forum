@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>Edit Post</h2>
	<fieldset>
		<legend>{{ $post->thread->title }}</legend>
		<form action="{{ route('app.forum.post.update', $post) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<textarea name="body" id="body" cols="30" rows="10">{{ $post->body }}</textarea><br>
			<button type="submit">Submit</button>
		</form>
	</fieldset>
@endsection