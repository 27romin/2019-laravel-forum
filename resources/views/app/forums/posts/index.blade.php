@foreach($posts as $post)
	{{$post->body}} - {{$post->user->name}} 
		@if(auth()->check())
			@if($post->user->id == auth()->user()->id)
				<a href="{{ route('app.forum.post.edit', $post) }}">edit</a>
				@if($post->id != $first->id)
					<form action="{{ route('app.forum.post.destroy', $post) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit">delete</button>
					</form>
				@endif
			@endif
		@endif
	<hr>
@endforeach

@if ($posts->hasMorePages() or $posts->lastItem())
	<p>
		{{ $posts->links() }}
	</p>
@endif