@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>View Thread</h2>

	@include('app.templates.partials.messages.success')

	@if(auth()->check())
		@if($first->user->id == auth()->user()->id)
			<p>
				<a href="{{ route('app.forum.move.thread.edit', $thread) }}">Move Thread</a>
				<form action="{{ route('app.forum.thread.destroy', $thread) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button type="submit">Delete Thread</button>
				</form>
			</p>
		@endif
	@endif

	<fieldset>
		<legend>{{ $thread->title }}</legend>
		@include('app.forums.posts.index')
	</fieldset>

	@if(auth()->check())
		@include('app.forums.posts.create')
	@endif
	
	@if ($posts->hasMorePages() or $posts->lastItem())
		<p>
    		{{ $posts->links() }}
    	</p>
	@endif

@endsection