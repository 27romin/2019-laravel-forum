@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>Move Thread</h2>
	@include('app.templates.partials.messages.errors')
	<fieldset>
		<legend>Forums</legend>
		<form action="{{ route('app.forum.move.thread.update', $thread) }}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<select name="forum_id" id="forum_id">
				@foreach($forums as $forum)
					@if($forum->id != $thread->forum->id)
						<option value="{{ $forum->id }}">{{ $forum->name }}</option>
					@endif
				@endforeach
			</select>
			<button type="submit">Submit</button>
		</form>
	</fieldset>
@endsection