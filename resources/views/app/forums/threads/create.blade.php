@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>Create Thread</h2>
	@include('app.templates.partials.messages.errors')
	<form action="{{ route('app.forum.thread.store', $forum) }}" method="POST">
		{{ csrf_field() }}

		<fieldset>
			<legend>Title</legend>
			<input type="text" id="title" name="title" value="{{ old('title') ? old('title') : '' }}">
		</fieldset>

		<fieldset>
			<legend>Body</legend>
			<textarea name="body" id="body" cols="30" rows="10">{{ old('body') ? old('body') : '' }}</textarea>
		</fieldset>
		
		<button type="submit">Submit</button>
	</form>
@endsection