@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>Forums</h2>
	@foreach ($categories as $category) 
		<fieldset>
			<legend>{{ $category->name }}</legend>
			@foreach ($category->forums as $forum) 
				<a href="{{ route('app.forum.show', $forum->id) }}">{{ $forum->name }}</a>
					@if($forum->latestPost)
					- 
					In: {{ $forum->latestPost->thread->title }},
					Updated at: {{ $forum->latestPost->thread->updated_at }},
					By: {{ $forum->latestPost->thread->user->name }}
					@endif
				<hr>
			@endforeach
		</fieldset>
	@endforeach
@endsection
