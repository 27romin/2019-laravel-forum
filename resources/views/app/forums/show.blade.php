@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
	<h2>Sub Foums</h2>

	@if(!$forum->subForums->isEmpty())
		<fieldset>
			@foreach($forum->subForums as $subforum)
				<a href="{{ route('app.forum.show', $subforum->forum->id) }}">{{ $subforum->forum->name }}</a>
					@if($subforum->forum->latestPost)
					- 
					In: {{ $subforum->forum->latestPost->thread->title }},
					Updated at: {{ $subforum->forum->latestPost->thread->updated_at }},
					By: {{ $subforum->forum->latestPost->thread->user->name }}
					@endif
				<hr>
			@endforeach
		</fieldset>
	@endif

	<h2>View Forum</h2>
	@if(auth()->check())
		<p>
			<a href="{{ route('app.forum.thread.create', $forum) }}">Create Thread</a>
		</p>
	@endif
	<fieldset>
		<legend>{{ $forum->name }}</legend>
		@forelse($threads as $thread)
			<a href="{{ route('app.forum.thread.show', $thread) }}">{{ $thread->title }}</a> 
			- Posted by {{ $thread->user->name }},
			Updated at {{ $thread->updated_at }}
			<hr>
		@empty
			No threads posted.
		@endforelse
	</fieldset>
	@if(auth()->check())
		<p>
			<a href="{{ route('app.forum.thread.create', $forum) }}">Create Thread</a>
		</p>
	@endif
	
	@if($threads->hasMorePages() or $threads->lastItem())
		<p>
    		{{ $threads->links() }}
    	</p>
	@endif
	
@endsection