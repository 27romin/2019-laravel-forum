<fieldset>
	@guest
	    <a href="{{ route('login') }}">Login</a>
	    <a href="{{ route('register') }}">Register</a>
	@else
		<legend>Welcome {{ auth()->user()->name }}</legend>
 		@if(auth()->user()->administrator)
	        <a href="{{ route('admin.home.index') }}">Admin Panel</a>
	    @endif
    	<a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>
    	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
	@endguest
</fieldset>

<fieldset>
	<legend>Navigation</legend>
	<a href="{{ route('app.home.index') }}">Home</a>
	<a href="{{ route('app.forum.index') }}">Forum</a>
</fieldset>