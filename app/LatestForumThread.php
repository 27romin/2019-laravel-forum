<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LatestForumThread extends Model
{
	protected $fillable = [
		'forum_id', 'thread_id'
	];


    /**
     * A latest forum belongs to a thread.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
    	return $this->belongsTo(ForumThread::class, 'thread_id', 'id');
    }

    /**
     * A latest forum belongs to a forum.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forum()
    {
    	return $this->belongsTo(Forum::class, 'forum_id', 'id');
    }
}
