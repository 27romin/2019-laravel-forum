<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $fillable = [
    	'name', 'description', 'position', 'category_id',
    ];

    /**
     * A forum belongs to a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
    	return $this->belongsTo(ForumCategory::class, 'category_id', 'id');
    }

    /**
     * A forum can have many threads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(ForumThread::class, 'forum_id', 'id')->with('user');
    }

    /**
     * A forum can have many subforums.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subForums()
    {
        return $this->hasMany(SubForum::class, 'parent_id', 'id');
    }

    /**
     * Get all forums that is not a subforum.
     *
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function nonSubForums()
    {
        return static::whereNotIn('id', $this->subforums->pluck('child_id'))->get();
    }

    /**
     * A forum has one latest post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function latestPost()
    {
        return $this->hasOne(LatestForumThread::class, 'forum_id', 'id');
    }
}
