<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    protected $fillable = [
    	'name', 'description', 'position',
    ];

    protected $with = [
        'forums', 
        'forums.subforums',
        'forums.latestPost.thread.user',
    ];

    /**
     * A category can have many forums.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forums()
    {
    	return $this->hasMany(Forum::class, 'category_id', 'id')->orderby('position', 'asc');
    }
}
