<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubForum extends Model
{
    protected $fillable = [
    	'parent_id', 'child_id',
    ];

    protected $with = ['forum'];

    /**
     * A subforum belongs to a forum.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forum()
    {
    	return $this->belongsTo(Forum::class, 'child_id', 'id');
    }
}
