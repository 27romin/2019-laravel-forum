<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forum;
use App\ForumThread;
use App\ForumPost;
use App\LatestForumThread;

class ForumThreadController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Forum $forum 
     * @param  ForumThread $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Forum $forum, ForumThread $thread)
    {
    	$posts = $thread->posts()->with('user')->paginate(10);
        $first = $posts->first();
    	return view('app.forums.threads.show', compact('forum', 'thread', 'posts', 'first'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function create(Forum $forum)
    {
        return view('app.forums.threads.create', compact('forum'));
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function store(Forum $forum)
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
    
        $thread = ForumThread::create([
            'title' => request()->title,
            'user_id' => auth()->user()->id,
            'forum_id' => $forum->id
        ]);

        $post = ForumPost::create([
        	'body' => request()->body,
        	'user_id' => auth()->user()->id,
            'thread_id' => $thread->id
        ]);

        LatestForumThread::updateOrCreate(
            ['forum_id' => $forum->id],
            ['thread_id' => $thread->id]
        );
        
        return redirect()->route('app.forum.show', $thread->forum)->withSuccess('Thread Created.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ForumThread $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForumThread $thread)
    {
        $thread->delete();

        $latest = $thread->forum->threads()->orderBy('updated_at', 'desc')->first();
        
        if($latest) {
            LatestForumThread::updateOrCreate(
                ['forum_id' => $thread->forum->id],
                ['thread_id' => $latest->id]
            );
        }

        return redirect()->route('app.forum.show', $thread->forum)->withSuccess('Thread Deleted.');
    }
}
