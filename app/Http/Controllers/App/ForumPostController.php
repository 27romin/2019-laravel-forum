<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forum;
use App\ForumThread;
use App\ForumPost;
use App\LatestForumThread;

class ForumPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ForumThread $thread
     * @return \Illuminate\Http\Response
     */
    public function store(ForumThread $thread)
    {
        request()->validate([
            'body' => 'required',
        ]);
    
        $post = ForumPost::create([
            'body' => request()->body,
            'user_id' => auth()->user()->id,
            'thread_id' => $thread->id,
        ]);

        LatestForumThread::updateOrCreate(
            ['forum_id' => $thread->forum->id],
            ['post_id' => $post->id]
        );
    
        return redirect()->route('app.forum.thread.show', $thread)->withSuccess('Post Created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ForumPost $post
     * @return \Illuminate\Http\Response
     */
    public function edit(ForumPost $post)
    {
        return view('app.forums.posts.edit', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ForumPost $post
     * @return \Illuminate\Http\Response
     */
    public function update(ForumPost $post)
    {
        request()->validate([
            'body' => 'required',
        ]);
    
        $post->update(request()->all());

        return redirect()->route('app.forum.thread.show', $post->thread)->withSuccess('Post Created.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ForumPost $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForumPost $post)
    {
        $post->delete();

        LatestForumThread::updateOrCreate(
            ['forum_id' => $post->thread->forum->id],
            ['post_id' => $post->thread->posts->last()->id]
        );

        return redirect()->route('app.forum.thread.show', $post->thread)->withSuccess('Post Deleted.');
    }
}
