<?php
namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forum;
use App\ForumCategory;

class ForumController extends Controller
{
	/**
	  * Display a listing of the resource.
	  *
	  * @return \Illuminate\Http\Response
	  */
	public function index()
	{
		$categories = ForumCategory::orderby('position', 'asc')->get();
		return view('app.forums.index', compact('categories'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  Forum $forum 
	 * @return \Illuminate\Http\Response
	 */
	public function show(Forum $forum)
	{
		$threads = $forum->threads()->orderBy('updated_at', 'desc')->paginate(10);
		return view('app.forums.show', compact('forum', 'threads'));
	}  
}
