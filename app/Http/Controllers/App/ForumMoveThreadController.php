<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ForumThread;
use App\Forum;
use App\LatestForumThread;

class ForumMoveThreadController extends Controller
{
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  ForumThread $thread
	 * @return \Illuminate\Http\Response
	 */
	public function edit(ForumThread $thread)
	{
	    $forums = Forum::get();
        return view('app.forums.threads.move.edit', compact('thread', 'forums'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  ForumThread $thread
     * @return \Illuminate\Http\Response
     */
    public function update(ForumThread $thread)
    {
        request()->validate([
            'forum_id' => 'required',
        ]);

        /**
         * Store the forum object before the thread is 
         * moved.
         */
        $forum = $thread->forum;

        /**
         * Move the thread to new a forum.
         */
        LatestForumThread::updateOrCreate(
            ['forum_id' => request()->forum_id],
            ['thread_id' => $thread->id]
        );

        $thread->update(request()->all());

        /**
         * Change thread id to the latest updated thread.
         */
        $latest = $forum->threads()->orderBy('updated_at', 'desc')->first();
        if($latest){
            $thread->forum->latestPost->update(['thread_id' => $latest->id ]);
        }

        /**
         * If current forum threads are empty, 
         * then remove its last post record.
         */
        if($forum->threads->isEmpty()) {
            $thread->forum->latestPost->delete();
        }else {

            /**
             * Change thread id to the latest updated post.
             */
            $latest = $thread->forum->threads()->orderBy('updated_at', 'desc')->first();
            $thread->forum->latestPost->update(['thread_id' => $latest->id ]);
        }

        return redirect()->route('app.forum.thread.show', $thread)->withSuccess('Thread Moved.');
    }
}
