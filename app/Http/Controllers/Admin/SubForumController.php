<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Forum;
use App\SubForum;

class SubForumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:administrator');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Forum $forum)
    {
        $nonSubForums = $forum->nonSubForums();
        return view('admin.forums.subforums.create', compact('forum', 'nonSubForums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Forum $forum)
    {
        request()->validate([
            'child_id' => 'required',
        ]);
    
        SubForum::create([
            'parent_id' => $forum->id,
            'child_id' => request()->child_id,
        ]);
    
        return redirect()->route('admin.forum.index')->withSuccess('Subforum Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  SubForum $subforum 
     * @return \Illuminate\Http\Response
     */
    public function show(SubForum $subforum)
    {
        return view('admin.forums.subforums.show', compact('subforum'));
    }  

    /**
     * Remove the specified resource from storage.
     *
     * @param  SubForum $subforum 
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubForum $subforum)
    {
        $subforum->delete();
        return redirect()->route('admin.forum.index')->withSuccess('Subforum Deleted.');
    }
}
