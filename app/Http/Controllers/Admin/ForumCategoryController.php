<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ForumCategory;

class ForumCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:administrator');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.forums.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'position' => 'required|integer|unique:forum_categories|between:1,100',
        ]);
    
        ForumCategory::create(request()->all());
    
        return redirect()->route('admin.forum.index')->withSuccess('Category Created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ForumCategory $category
     * @return \Illuminate\Http\Response
     */
    public function edit(ForumCategory $category)
    {
        return view('admin.forums.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ForumCategory $category 
     * @return \Illuminate\Http\Response
     */
    public function update(ForumCategory $category)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'position' => 'required|integer|between:1,100|unique:forum_categories,position,'.$category->id,
        ]);
    
        $category->update(request()->all());
        return redirect()->route('admin.forum.index')->withSuccess('Category Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ForumCategory $category 
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForumCategory $category)
    {
        $category->delete();
        return redirect()->route('admin.forum.index')->withSuccess('Category Deleted.');
    }
}
