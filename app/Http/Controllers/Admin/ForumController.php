<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Forum;
use App\ForumCategory;

class ForumController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:administrator');
    }

    /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
    public function index()
    {
		$categories = ForumCategory::orderby('position', 'asc')->get();
        return view('admin.forums.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = ForumCategory::get();
        return view('admin.forums.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'position' => ['required', 'integer', 'between:1,100', Rule::unique('forums')->where(function ($query) use ($request) {
                    return $query->where('position', $request->position)->where('category_id', $request->category_id);
                }),
            ],
        ]);
    
        Forum::create(request()->all());
    
        return redirect()->route('admin.forum.index')->withSuccess('Forum Created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function edit(Forum $forum)
    {
        $categories = ForumCategory::get();
        return view('admin.forums.edit', compact('forum', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Forum $forum 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forum $forum)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'position' => ['required', 'integer', 'between:1,100', Rule::unique('forums')->where(function ($query) use ($request, $forum) {
                    return $query->where('position', '!=', $forum->position)->where('category_id', $request->category_id);
                }),
            ],
        ]);
    
        $forum->update(request()->all());
    
        return redirect()->route('admin.forum.index')->withSuccess('Forum Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Forum $forum 
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forum $forum)
    {
        $forum->delete();
        return redirect()->route('admin.forum.index')->withSuccess('Forum Deleted.');
    }
}
