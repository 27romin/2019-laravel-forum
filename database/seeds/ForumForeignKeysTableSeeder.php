<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/**
    	 * --------------------------------------
    	 * Forum Categories
    	 *
    	 * forum_categories/forums - Remove forum on delete
    	 */
        Schema::table('forums', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('forum_categories')->onDelete('cascade');
        });

    	/**
    	 * --------------------------------------
    	 * Users, Forums
    	 *
    	 * users/forum_threads - Remove thread when user is removed
    	 * forums/forum_threads - Remove thread when forum is removed
    	 */
        Schema::table('forum_threads', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
        });

        /**
    	 * --------------------------------------
    	 * Forum Threads
    	 *
    	 * forum_threads/forum_posts - Remove post when thread is removed
    	 */
        Schema::table('forum_posts', function (Blueprint $table) {
            $table->foreign('thread_id')->references('id')->on('forum_threads')->onDelete('cascade');
        });


    	/**
    	 * --------------------------------------
    	 * Forums
    	 *
    	 * forums/sub_forums - Remove when forum is removed
    	 * forums/sub_forums - Remove when forum is removed
    	 */
        Schema::table('sub_forums', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('forums')->onDelete('cascade');
            $table->foreign('child_id')->references('id')->on('forums')->onDelete('cascade');
        });

    	/**
    	 * --------------------------------------
    	 * Forums, Forum Threads
    	 *
    	 * forums/latest_forum_threads - Remove when thread is removed
    	 * forum_threads/latest_forum_threads - Remove when forum is removed
    	 */
        Schema::table('latest_forum_threads', function (Blueprint $table) {
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->foreign('thread_id')->references('id')->on('forum_threads')->onDelete('cascade');
        });
    }
}
