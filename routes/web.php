<?php


Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('home', 'HomeController@index')->name('admin.home.index');

	Route::resource('forum', 'ForumController', ['as' => 'admin'])->except(['show']);
    Route::resource('/forum/category', 'ForumCategoryController', ['as' => 'admin.forum'])->except(['show']);

    Route::get('/forum/{forum}/subforum/create', 'SubForumController@create')->name('admin.forum.subforum.create');
    Route::post('/forum/{forum}/subforum', 'SubForumController@store')->name('admin.forum.subforum.store');
    Route::get('/forum/subforum/{subforum}', 'SubForumController@show')->name('admin.forum.subforum.show');
    Route::delete('/forum/subforum/{subforum}', 'SubForumController@destroy')->name('admin.forum.subforum.destroy');
});

Route::namespace('App')->prefix('home')->group(function () {
    Route::get('', 'HomeController@index')->name('app.home.index');

    Route::get('/forum', 'ForumController@index')->name('app.forum.index');
    Route::get('/forum/{forum}', 'ForumController@show')->name('app.forum.show');
    
    Route::get('/forum/{forum}/thread/create', 'ForumThreadController@create')->name('app.forum.thread.create');
    Route::get('/thread/{thread}', 'ForumThreadController@show')->name('app.forum.thread.show');
    Route::post('/forum/{forum}/thread', 'ForumThreadController@store')->name('app.forum.thread.store');
    Route::delete('/thread/{thread}', 'ForumThreadController@destroy')->name('app.forum.thread.destroy');
    Route::post('/thread/{thread}/post', 'ForumPostController@store')->name('app.forum.post.store');

    Route::patch('/post/{post}', 'ForumPostController@update')->name('app.forum.post.update');
    Route::delete('/post/{post}', 'ForumPostController@destroy')->name('app.forum.post.destroy');
    Route::get('/post/{post}/edit', 'ForumPostController@edit')->name('app.forum.post.edit');

    Route::resource('/move/thread', 'ForumMoveThreadController', ['as' => 'app.forum.move'])->only(['edit', 'update']);
	Auth::routes();
});